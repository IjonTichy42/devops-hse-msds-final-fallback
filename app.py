import pandas as pd
from flask import Flask
import os

basePath = os.path.dirname(os.path.abspath(__file__))

artists = pd.read_json(basePath + "/yandex_music/artists.jsonl", orient='records', lines=True)
events = pd.read_csv(basePath + "/yandex_music/events.csv")

app = Flask(__name__)

@app.route('/')
@app.route('/<path:path>')
def get_artist(path=''):
    return events.merge(artists)[['artistName', 'plays']].groupby("artistName").sum().sort_values('plays', ascending=False).head(1).iloc[0].name


if __name__ == '__main__':
    app.run("0.0.0.0")